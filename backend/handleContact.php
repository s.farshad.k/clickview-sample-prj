<?php
error_reporting(E_ERROR | E_PARSE);
header('content-type: application/json; charset=utf-8');
header("access-control-allow-origin: *");

try {
    $hostname = "localhost:3308";
    $username = "root";
    $password = "";
    $database = "clcikView";
    $mysqli = new mysqli($hostname, $username, $password, $database);
    /* check connection */
    if (mysqli_connect_errno()) {
        echo json_encode(["status"=>"error","errors"=>"database is not connected!"]);
        // printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    }
    // Takes raw data from the request
    $json = file_get_contents('php://input');
    // Converts it into a PHP object
    $data = json_decode($json);

    //magic quotes logic
    if (get_magic_quotes_gpc())
    {
        function stripslashes_deep($value)
        {
            $value = is_array($value) ?
            array_map('stripslashes_deep', $value) :
            stripslashes($value);
            return $value;
        }
        
        $data = array_map('stripslashes_deep', $data);
        $_POST = array_map('stripslashes_deep', $_POST);
        $_GET = array_map('stripslashes_deep', $_GET);
        $_COOKIE = array_map('stripslashes_deep', $_COOKIE);
        $_REQUEST = array_map('stripslashes_deep', $_REQUEST);
    } 
    if(strlen($data->name)==0 || strlen($data->mail)==0 || !ctype_digit($data->role) ||
         !ctype_digit($data->tel) || strlen($data->school)==0 || !ctype_digit($data->enquiry)){
            echo json_encode(["status"=>"error","errors"=>"There is some error with the input. Please check your input and try again."]);
            exit();
        }
    $sql = "INSERT INTO `clcikview`.`contact_us` (`name`, `mail`, `tel`, `rol`, `enquiry`, `school`, `more`) VALUES 
                        ('{$data->name}', '{$data->mail}', '{$data->tel}', {$data->role}, {$data->enquiry}, '{$data->school}', '{$data->more}');";
    if ($mysqli->query($sql) === TRUE) {
        echo json_encode(["status"=>"success"]);
    } else {
        print( $mysqli->error);
        echo json_encode(["status"=>"error","errors"=>"There is some error with the input. Please check your input and try again."]);        
    }                
} catch (Exception $e) {
    echo json_encode(["status"=>"error","errors"=>"Unknown!"]);
    exit();
}
