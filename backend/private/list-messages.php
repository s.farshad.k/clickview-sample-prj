<?php
error_reporting(E_ERROR | E_PARSE);
header('content-type: application/json; charset=utf-8');
header("access-control-allow-origin: *");
    // Takes raw data from the request
    $json = file_get_contents('php://input');
    // Converts it into a PHP object
    $data = json_decode($json);

    /*
    ** test for username/password
    */
    if( ( isset($data->user ) && ( $data->user == "admin" ) ) AND
      ( isset($data->pass ) && ( $data->pass == "admin" )) )
    {
        $hostname = "localhost:3308";
        $username = "root";
        $password = "";
        $database = "clcikView";
        $mysqli = new mysqli($hostname, $username, $password, $database);
        if (mysqli_connect_errno()) {
            echo json_encode(["status"=>"error","errors"=>"database is not connected!"]);
            // printf("Connect failed: %s\n", mysqli_connect_error());
            exit();
        }
        $sql = "SELECT * FROM clcikview.contact_us";
        $messages = $mysqli->query($sql);

        if ($messages && $messages->num_rows > 0) {
            $allMsgs = $messages->fetch_all(MYSQLI_ASSOC);
            echo json_encode(["status"=>"success", "messages"=>json_encode($allMsgs)]);
        } else {
            echo json_encode(["status"=>"error","errors"=>"There is some error with the database. Please try again later."]);        
        }                
    }
    else
    {
        //Send headers to cause a browser to request
        //username and password from user
        header("WWW-Authenticate: " .
            "Basic realm=\"Protected Area\"");
        header("HTTP/1.0 401 Unauthorized");

        //Show failure text, which browsers usually
        //show only after several failed attempts
    }
?>