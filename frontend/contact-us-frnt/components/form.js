import React from 'react'
import { fetchWithTimeOut } from '../components/fetchWithTimeOut';

class ContactForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name:"",
            mail:"",
            pos:"",
            role:"",
            school:"",
            tel:"",
            agreed:false,
        };
        this.handleChange = this.handleChange.bind(this);
        this.sendForm = this.sendForm.bind(this);
    }
    handleChange = event => {
        //TODO: add a file upload for user if had time.
        const field = event.target.id;
        this.setState({
          [field]: event.target.value,
        });
      };
      sendForm=(e)=>{
        e.preventDefault();
        console.log(this.state);
        if(this.state.agreed!="on"){
            alert("You should agree with our terms and services");
            return;
        }
        const url = `http://localhost/handleContact.php`;
        const data = {
            name: this.state.name,
            mail: this.state.mail,
            tel: this.state.tel,
            agreed: this.state.agreed,
            role: this.state.role,
            enquiry: this.state.enquiry,
            school: this.state.school,
            isLoading:false,
          };
        const options = {
            method: 'POST',
            body: JSON.stringify(data),
            // headers: {"Authorization": "basic u:p"},
          };
          fetchWithTimeOut(
            url,
            options,
            response => {
              if (response.status !== 'success') {
                alert("There is a problem with your data:\n"+response.errors);
              } else {
                alert("You request has been successfully submitted!");      
              }
            },
            error => {
              console.log(error);
            },
          );
        }
    render() {
        return ( <React.Fragment>
<form onSubmit={this.sendForm}>
  <div className="row">
    <div className="col-md-6 col-sm-12">
    <div className="form-group">
        <input type="text" className="form-control" aria-describedby="emailHelp" placeholder="Name" id="name" onChange={this.handleChange}/>
    </div>
    <div className="form-group">
        <input type="email" className="form-control" aria-describedby="emailHelp" placeholder="Email" id="mail" onChange={this.handleChange}/>
        <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
    </div>
    <div className="form-group">
        <input type="tel" className="form-control" aria-describedby="phoneHelp" placeholder="Phone" id="tel" pattern="[0-9]{10}" onChange={this.handleChange}/>
    </div>
    <div className="form-group">
        <select className="form-control" defaultValue="" id="role" onChange={this.handleChange}>
            <option value="" data-default >Position</option>
            <option value="1">Teacher</option>
            <option value="2">Lecturer</option>
            <option value="3">Head of Department</option>
            <option value="4">Principal</option>
            <option value="5">Deputy Principal</option>
            <option value="6">Assistant Principal</option>
            <option value="7">eLearning</option>
            <option value="8">Librarian</option>
            <option value="9">ICT</option>
            <option value="10">Student</option>
            <option value="11">Other</option>
        </select>
    </div>
    <div className="form-group">
        <input type="text" className="form-control" placeholder="school name" id="school" onChange={this.handleChange}/>
    </div>
    </div>
    <div className="col-md-6 col-sm-12">

    <div className="form-group">
    <select className="form-control " required="" defaultValue=""  id="enquiry" onChange={this.handleChange}>
        <option disabled="" data-default>My enquiry is a:</option>
        <option value="1">Technical enquiry</option>
        <option value="2">General enquiry</option>
        <option value="3">Account enquiry</option>
        <option value="4">School training enquiry</option>
        <option value="5">Other</option>
    </select>
    </div>  
    <div className="form-group" id="more">
        <textarea  className="form-control" rows="6" placeholder="Additional Questions/Comments (Optional)" spellCheck="false" id="more" onChange={this.handleChange}></textarea> 
    </div>

<div className="form-check form-group" id="aggreed">
    <label className="form-check-label" ><input type="checkbox" className="form-check-input" id="agreed" onChange={this.handleChange}/>I agree to the Terms and Conditions and Privacy Policy</label>
</div>
<div className="form-group" id="more">
    <button type="submit" className="btn btn-warning" >Contact Us</button>
</div>    
</div>

</div>

</form>
        <style jsx>{`
            select option[data-default] {
                display:none;
                color:#c7c7c7;
            }
        `}</style>
        </React.Fragment>);
    }
}
export default ContactForm