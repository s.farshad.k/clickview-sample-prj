// function timeout(ms, promise) {
//   return new Promise((resolve, reject) => {
//     setTimeout(() => {
//       reject(new Error('timeout'));
//     }, ms);
//     promise.then(resolve, reject);
//   });
// }

// export { timeout as Wrapper };
import { toastr } from 'react-redux-toastr';

const REQUEST_TIMEOUT=50000;
const fetchWithTimeOut = (
  url,
  options,
  resolveCallback,
  rejectCallback,
  pageErrorCallback=false,
  customeTimeOut = false,
) => {
  let isTimedOut = false;
  // this is for time out on fetch
  new Promise((resolve, reject) => {
    const timeOut = setTimeout(() => {
      isTimedOut = true;
      reject("Time out!");
    }, customeTimeOut ? customeTimeOut : REQUEST_TIMEOUT);

    // START of fetching data
    fetch(url, options)
      // fetch was successful
      .then(response => response.json())
      .then(data => {
        clearTimeout(timeOut);
        resolve(data);
      })
      .catch(error => {
        if (isTimedOut) return;
        clearTimeout(timeOut);
        if(!pageErrorCallback)
            alert("Request Error!");
        else
            pageErrorCallback(error);
        // TODO: check the response Header and handle Errors
        console.log('this is error from fetchWithTime ', error);
        reject(error);
      });
    // END of fetching data
  })
    .then(data => {
      // this happens on data being fetched
      resolveCallback(data);
    })
    .catch(error => {
      if (isTimedOut) {
        // toastr.error("Error!", 'Request time out!');
        // console.log("clb:",pageErrorCallback);
        if(!pageErrorCallback)
            alert("Request Time out!");
        else
            pageErrorCallback(error);
      }
      // this happens on TIME_OUT
      rejectCallback(error);
    });
};

export { fetchWithTimeOut };
