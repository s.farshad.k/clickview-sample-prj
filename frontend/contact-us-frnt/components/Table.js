import React from 'react'
import { fetchWithTimeOut } from '../components/fetchWithTimeOut';
import { Spinner } from './loading';

const url = `http://localhost/private/list-messages.php`;
const createSearchableTable = ()=>{
    setTimeout(()=>{
        if(jQuery("#table").length){
            $( '#table' ).searchable({
                striped: true,
                oddRow: { 'background-color': '#f5f5f5' },
                evenRow: { 'background-color': '#fff' },
                searchType: 'fuzzy'
            });
        }else{
            setTimeout(createSearchableTable,100);
        }
    })
}
class Table extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            records:[],
            isLoading:false,
            user:"",
            pass:"",
            isLoggedIn:false
        };
        this.handleChange = this.handleChange.bind(this);
        this.fetchList = this.fetchList.bind(this);

    }
    componentWillMount(){
        if(typeof window !== 'undefined'){//means we are on the client side
            if(localStorage.getItem('user')!=null)
                this.setState({user:localStorage['user']})
            if(localStorage.getItem('pass')!=null)
                this.setState({user:localStorage['pass']})
        }
    }
    handleChange = event => {
        //TODO: add a file upload for user if had time.
        const field = event.target.id;
        this.setState({
          [field]: event.target.value,
        });
      };

    fetchList(e){
        let base64 = require('base-64');
        e.preventDefault();
        this.setState({isLoading:true});
        const options = {
            method: 'POST',
            body: JSON.stringify({
                user: this.state.user,
                pass: this.state.pass,
            }),
        };
        console.log(options);
        fetchWithTimeOut(
            url,
            options,
            response => {
                if (response.status === 'success') {
                    alert("You logged in successfully");
                    localStorage.setItem("user",this.state.user);
                    localStorage.setItem("pass",this.state.pass);
                    this.setState({records: JSON.parse(response.messages),isLoading:false, isLoggedIn: true},createSearchableTable);      
                }else{
                    console.log("err!");
                    alert("The username and password you provided is not correct. Please check your inputs and try again.");
                    this.setState({
                        isLoading:false
                    });            
                }
            },
            error => {
                console.log(error);
            },
            error=>{
                this.setState({
                    isLoading:false
                });            
                console.log("err:",error);
                alert("The username and password you provided is not correct. Please check your inputs and try again.");
            }
    
        );        
    }
    componentDidUpdate(){
        console.log("new update!");
    }
    componentDidMount(){                
        if(localStorage.getItem('user')!=null && localStorage.getItem('pass')!=null){
            this.setState({
                isLoading:true
            });
            const options = {
                method: 'POST',
                body: JSON.stringify({
                    user: localStorage.getItem('user'),
                    pass: localStorage.getItem('pass'),
                }),
            };
            fetchWithTimeOut(
                url,
                options,
                response => {
                    if (response.status === 'success') {
                        this.setState({records: JSON.parse(response.messages),isLoading:false, isLoggedIn: true},createSearchableTable);      
                    }else{
                        this.setState({
                            isLoading:false
                        });            
                    }
                },
                error => {
                    console.log(error);
                }
            );        
        }
    }
    render(){
        if(this.state.isLoading){
            return (<React.Fragment>
                <Spinner />
                </React.Fragment>);
        }else if(!this.state.isLoggedIn){
            return (
                <React.Fragment>
                <form onSubmit={this.fetchList} className="loginForm">
                    <div className="form-group">
                        <input type="text" className="form-control" placeholder="username" id="user" onChange={this.handleChange}/>
                    </div>
                    <div className="form-group">
                        <input type="password" className="form-control" placeholder="password" id="pass" onChange={this.handleChange}/>
                    </div>
                    <div className="form-check form-group" id="aggreed">
                        <label className="form-check-label" ><input type="checkbox" className="form-check-input" id="remember" onChange={this.handleChange}/>Remember me!</label>
                    </div>
                    <div className="form-group" id="more">
                        <button type="submit" className="btn btn-warning" >Login</button>
                    </div>    
                </form>
                <style jsx>{`
                    .loginForm{
                        width:50%;
                        margin:20% auto;
                        text-align:center;

                    }

                `}</style>
                </React.Fragment>
            )
        }else{
            console.log(this.state.records);
            if(typeof window !== 'undefined'){//means we are on the client side
                return (
            <React.Fragment>
            <div className="listContainer">
                <div className="row">
                    <div className="col-lg-4 col-lg-offset-4 form-group">
                        <input type="search" id="search"  className="form-control" placeholder="Search whole table" />
                    </div>
                </div>
                <table className="table" id="table">
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>name</th>
                            <th>email</th>
                            <th>phone</th>
                            <th>position</th>
                            <th>enquiry</th>
                            <th>school</th>
                            <th>additional comments</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.records.map((val,key)=>
                        <tr>
                            <td>{val.id}</td>
                            <td>{val.name}</td>
                            <td>{val.mail}</td>
                            <td>{val.tel}</td>
                            <td>{val.rol}</td>
                            <td>{val.enquiry}</td>
                            <td>{val.school}</td>
                            <td>{val.more}</td>
                        </tr>
                        )}
                    </tbody>
                </table>
            </div>
            <style jsx>{`
            .listContainer{
                width:80%;
                margin:0 auto;
                text-align:center;

            }

            `}</style>
        </React.Fragment>);
            }
        }
    }
}
export default Table;
