
import React from 'react';

class Spinner extends React.Component {
  render() {
    return (
      <React.Fragment>
        <div id="loader">
            <div className="folding-cube">
            <div className="cube1 cube" />
            <div className="cube2 cube" />
            <div className="cube4 cube" />
            <div className="cube3 cube" />
            </div>
        </div>
        <style jsx>
        {`

            #loader {
            width: 100%;
            height: 100%;
            position: fixed;
            z-index: 10000;
            background: #fff;
            top: 0;
            right: 0;
            left: 0;
            }

            .spinner {
            margin: auto;
            position: absolute;
            top: 0;
            bottom: 0;
            right: 0;
            left: 0;
            width: 50px;
            height: 50px;
            text-indent: -9999em;
            border-radius: 50%;
            -webkit-transform: translateZ(0);
            -ms-transform: translateZ(0);
            transform: translateZ(0);
            -webkit-animation: spinner-anim 0.8s infinite linear;
            animation: spinner-anim 0.8s infinite linear;
            }

            @-webkit-keyframes spinner-anim {
            0% {
                -webkit-transform: rotate(0deg);
                transform: rotate(0deg);
            }

            100% {
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg);
            }
            }

            @keyframes spinner-anim {
            0% {
                -webkit-transform: rotate(0deg);
                transform: rotate(0deg);
            }

            100% {
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg);
            }
            }

            .scaleout,
            .double-bounce {
            width: 50px;
            height: 50px;
            position: absolute;
            margin: auto;
            top: 0;
            bottom: 0;
            right: 0;
            left: 0;
            }

            .double-bounce1,
            .double-bounce2 {
            width: 100%;
            height: 100%;
            border-radius: 100%;
            background: #333;
            position: absolute;
            top: 0;
            left: 0;
            opacity: 0.8;
            -webkit-animation: sk-bounce 2s infinite ease-in-out;
            animation: sk-bounce 2s infinite ease-in-out;
            }

            .double-bounce2 {
            -webkit-animation-delay: -1s;
            animation-delay: -1s;
            }

            @-webkit-keyframes sk-bounce {

            0%,
            100% {
                -webkit-transform: scale(0)
            }

            50% {
                -webkit-transform: scale(1)
            }
            }

            @keyframes sk-bounce {

            0%,
            100% {
                -webkit-transform: scale(0);
                transform: scale(0);
            }

            50% {
                -webkit-transform: scale(1);
                transform: scale(1);
            }
            }

            .scaleout {
            opacity: 1;
            background: #333;
            border-radius: 50%;
            -webkit-animation: sk-scaleout 1s infinite ease-in-out;
            animation: sk-scaleout 1s infinite ease-in-out;
            }

            @-webkit-keyframes sk-scaleout {
            0% {
                -webkit-transform: scale(0)
            }

            100% {
                -webkit-transform: scale(1);
                opacity: 0;
            }
            }

            @keyframes sk-scaleout {
            0% {
                -webkit-transform: scale(0);
                transform: scale(0);
            }

            100% {
                -webkit-transform: scale(1);
                transform: scale(1);
                opacity: 0;
            }
            }

            .bounces {
            margin: auto;
            text-align: center;
            width: 60px;
            height: 20%;
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            }

            .bounces>div {
            width: 15px;
            height: 15px;
            background-color: #333;
            border-radius: 100%;
            display: inline-block;
            -webkit-animation: sk-bouncedelay 1.4s infinite ease-in-out both;
            animation: sk-bouncedelay 1.4s infinite ease-in-out both;
            }

            .bounces .bounce1 {
            -webkit-animation-delay: -0.32s;
            animation-delay: -0.32s;
            }

            .bounces .bounce2 {
            -webkit-animation-delay: -0.16s;
            animation-delay: -0.16s;
            }

            @-webkit-keyframes sk-bouncedelay {

            0%,
            80%,
            100% {
                -webkit-transform: scale(0)
            }

            40% {
                -webkit-transform: scale(1)
            }
            }

            @keyframes sk-bouncedelay {

            0%,
            80%,
            100% {
                -webkit-transform: scale(0);
                transform: scale(0);
            }

            40% {
                -webkit-transform: scale(1);
                transform: scale(1);
            }
            }

            .folding-cube {
            position: absolute;
            top: 0;
            bottom: 0;
            right: 0;
            left: 0;
            margin: auto;
            width: 60px;
            height: 60px;
            -webkit-transform: rotateZ(45deg);
            transform: rotateZ(45deg);
            }

            .folding-cube .cube {
            float: left;
            width: 50%;
            height: 50%;
            position: relative;
            -webkit-transform: scale(1.1);
            -ms-transform: scale(1.1);
            transform: scale(1.1);
            }

            .folding-cube .cube:before {
            content: '';
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background: #333;
            -webkit-animation: sk-foldCubeAngle 2.4s infinite linear both;
            animation: sk-foldCubeAngle 2.4s infinite linear both;
            -webkit-transform-origin: 100% 100%;
            -ms-transform-origin: 100% 100%;
            transform-origin: 100% 100%;
            }

            .folding-cube .cube2 {
            -webkit-transform: scale(1.1) rotateZ(90deg);
            transform: scale(1.1) rotateZ(90deg);
            }

            .folding-cube .cube3 {
            -webkit-transform: scale(1.1) rotateZ(180deg);
            transform: scale(1.1) rotateZ(180deg);
            }

            .folding-cube .cube4 {
            -webkit-transform: scale(1.1) rotateZ(270deg);
            transform: scale(1.1) rotateZ(270deg);
            }

            .folding-cube .cube2:before {
            -webkit-animation-delay: 0.3s;
            animation-delay: 0.3s;
            }

            .folding-cube .cube3:before {
            -webkit-animation-delay: 0.6s;
            animation-delay: 0.6s;
            }

            .folding-cube .cube4:before {
            -webkit-animation-delay: 0.9s;
            animation-delay: 0.9s;
            }

            @-webkit-keyframes sk-foldCubeAngle {

            0%,
            10% {
                -webkit-transform: perspective(140px) rotateX(-180deg);
                transform: perspective(140px) rotateX(-180deg);
                opacity: 0;
            }

            25%,
            75% {
                -webkit-transform: perspective(140px) rotateX(0deg);
                transform: perspective(140px) rotateX(0deg);
                opacity: 1;
            }

            90%,
            100% {
                -webkit-transform: perspective(140px) rotateY(180deg);
                transform: perspective(140px) rotateY(180deg);
                opacity: 0;
            }
            }

            @keyframes sk-foldCubeAngle {

            0%,
            10% {
                -webkit-transform: perspective(140px) rotateX(-180deg);
                transform: perspective(140px) rotateX(-180deg);
                opacity: 0;
            }

            25%,
            75% {
                -webkit-transform: perspective(140px) rotateX(0deg);
                transform: perspective(140px) rotateX(0deg);
                opacity: 1;
            }

            90%,
            100% {
                -webkit-transform: perspective(140px) rotateY(180deg);
                transform: perspective(140px) rotateY(180deg);
                opacity: 0;
            }
            }

            /* Just IE9 */

            @media all and (min-width:0\0) and (min-resolution:0.001dpcm) {
            #loader * {
                display: none
            }

            #loader {
                display: block;
                border: none;
                background: #fff url(../images/AjaxLoader.gif) center center no-repeat;
                right: 0;
                left: 0;
                top: 0;
                bottom: 0;
                margin: auto;
            }
        }

    
        `}</style>
      </React.Fragment>
    );
  }
}
export { Spinner };