import React from 'react'
import {
    render
} from 'react-dom'

const position = [51.505, -0.09]
class ContactMap extends React.Component {
    constructor(props) {
        super(props);
        this.myRef = React.createRef();
    }
    componentDidMount(){
        var mymap = L.map('myContactMap').setView(position, 13);
        L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
            maxZoom: 18,
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
                '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
                'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            id: 'mapbox.streets'
    	}).addTo(mymap);
    }
    render() {
        return ( <React.Fragment>
            <div id="myContactMap" />
            <style jsx>{`
            #myContactMap {
                width: 100%;
                min-height: calc( 100% - 60px);
                position: absolute;
            }`
            }</style>
            </React.Fragment>
         );
    }
}

export default ContactMap