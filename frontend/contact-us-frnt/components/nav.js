import React from 'react'
import Link from 'next/link'


const Nav = () => (
  <nav className="navbar navbar-expand-md navbar-light fixed-top bg-light">
    <a className="navbar-brand" href="https://www.clickview.com.au/">
      <img src="/static/logo.svg" height="25px" alt="Clickview Logo" />
    </a>
    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
      <span className="navbar-toggler-icon"></span>
    </button>
    <div className="collapse navbar-collapse" id="navbarCollapse">
      <ul className="navbar-nav mr-auto">
        <li className="nav-item active">
          <a className="nav-link" href="https://www.clickview.com.au/why-videos/"> Why Clickview?<span className="sr-only">(current)</span></a>
        </li>
        <li className="nav-item">
          <a className="nav-link" href="https://www.clickview.com.au/why-videos/">Primary</a>
        </li>
        <li className="nav-item">
          <a className="nav-link " href="#">Secondary</a>
        </li>
        <li className="nav-item">
          <a className="nav-link " href="#">Tertiary</a>
        </li>
        <li className="nav-item">
          <a className="nav-link " href="#">Helping and training</a>
        </li>
        <li className="nav-item">
          <a className="nav-link " href="#">Contact Us</a>
        </li>

      </ul>
      <div className="btn-group btn-group-toggle" data-toggle="buttons">
        <button className="btn btn-outline-warning " type="button">Free trial</button>
        <button className="btn btn-outline-secondary " type="button">Sign in</button>
        </div>
    </div>
  </nav>
)

export default Nav
