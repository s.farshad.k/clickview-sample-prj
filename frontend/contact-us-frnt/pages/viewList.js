import Head from 'next/head'
import Nav from '../components/nav'
import Table from "../components/Table";

const List = () => (
    <React.Fragment>
        <Head>
        <title>View Messages</title>
        <link rel='icon' href='/static/favicon.ico' importance='low' />
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossOrigin="anonymous" />
        <link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css"
            integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
            crossOrigin=""/>
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <script src="//rawgithub.com/stidges/jquery-searchable/master/dist/jquery.searchable-1.0.0.min.js"></script>
        </Head>

        <Table />
    </React.Fragment>
);

export default List
