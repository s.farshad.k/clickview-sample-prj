import React from 'react'
import Head from 'next/head'
import Nav from '../components/nav'
import ContactMap from '../components/map'
import ContactForm from '../components/form';

const Home = () => (
  <div>
    <Head>
      <title>Contact Us</title>
      <link rel='icon' href='/static/favicon.ico' importance='low' />
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossOrigin="anonymous" />
      <link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css"
        integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
        crossOrigin=""/>
    </Head>

    <Nav />

    <div className='map'>
      <ContactMap />
    </div>
    <div className="contactForm">
        <ContactForm />
      </div>

    <style jsx>{`
      .contactForm{
        position:absolute;
        z-index:9999;
        max-height: calc( 100% - 70px);
        margin-left:40px;
        padding:20px;
        background: #ffffffb5;;
      }
      @media only screen and (max-width: 600px) {
        .contactForm {
          width: 100%;
          height: auto;
          margin: 0px;
          padding-bottom: 20px;
          position: relative;
        }
      }
      .map {
        width: 100%;
        color: #333;
        padding-top:60px;
      }
      .title {
        margin: 0;
        width: 100%;
        padding-top: 80px;
        line-height: 1.15;
        font-size: 48px;
      }
      .title,
      .description {
        text-align: center;
      }
      .row {
        max-width: 880px;
        margin: 80px auto 40px;
        display: flex;
        flex-direction: row;
        justify-content: space-around;
      }
      .card {
        padding: 18px 18px 24px;
        width: 220px;
        text-align: left;
        text-decoration: none;
        color: #434343;
        border: 1px solid #9b9b9b;
      }
      .card:hover {
        border-color: #067df7;
      }
      .card h3 {
        margin: 0;
        color: #067df7;
        font-size: 18px;
      }
      .card p {
        margin: 0;
        padding: 12px 0 0;
        font-size: 13px;
        color: #333;
      }
    `}</style>
  </div>
)

export default Home
